# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vas_remediation/map.jinja" import vasypd_settings with context %}

{# ================================
 # Rollback vasyp remediation
 # ===============================#}
{% for file in vasypd_settings.rollback.mount_files %}
rollback_{{ file }}:
  file.managed:
    - name: /etc/{{ file }}
    - source: {{ vasypd_settings.backup.location }}/{{ file }}
    - mode: 0644
    - owner: root
    - group: root
    - onlyif:
      test -f {{ vasypd_settings.backup.location }}
{% endfor %}

restore_vasyp_pkg:
  pkg.installed:
    - name: {{ vasypd_settings.pkg.name }}
    - unless:
      - rpm -q {{ vasypd_settings.pkg.name }}

{% for service in vasypd_settings.service.name %}
start_{{ service }}_service:
  service.running:
    - name: {{ service }}
    - reload: True
    - enable: True
{% endfor %}

{% for __file in vasypd_settings.rollback.files %}
{% set filename = salt['cmd.run']('echo '~__file~' | awk -F/ \'{print $NF}\'',python_shell=True) %}
restore_vasyp_{{ filename }}:
  file.managed:
    - name: {{ __file }}
    - source: {{ vasypd_settings.rollback.locaton}}/{{ filename }}
    - mode: 0644
    - owner: root
    - group: root
    - onlyif:
      test -f {{ vasypd_settings.backup.location }}
{% endfor %}
