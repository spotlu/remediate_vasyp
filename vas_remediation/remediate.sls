# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vas_remediation/map.jinja" import vasypd_settings with context %}

{# ================================
 # Remediate vasyp vulnerabilities
 # ===============================#}
{% for service in vasypd_settings.service.name -%}
{% if service == "vasypd" -%}
stop_{{ service }}_service:
  service.dead:
    - name: {{ service }}
    - enable: false
    - unless:
        - rpm -q {{ vasypd_settings.pkg.name }}
{% endif -%}
{% endfor -%}

remove_vasypd_pkg:
  pkg.removed:
    - name: {{ vasypd_settings.pkg.name }}
    - require:
      - service: stop_vasypd_service

{% if salt['cmd.retcode']('test -f /etc/home.map') == 0 %}
skip_policy_update:
  test.show_notification:
    - text: "/etc/home.map is already present"

{% else %}
remove_vasyp_dependencies:
  cmd.run:
    - name: sed -i '/^\export\/home \/etc\/home.map/d' /etc/auto.master
    - onlyif:
        - grep '/export/home' /etc/auto.master

copy_map_file:
  file.managed:
    - name: {{ vasypd_settings.config.name }}
    - source: {{ vasypd_settings.config.source }}
    - mode: 0644
    - owner: root
    - group: root

update_mount_policy:
  file.append:
    - name: {{ vasypd_settings.config.policyfile }}
    - text: /export/home /etc/home.map
    - require:
      - file: copy_map_file

{% for service in vasypd_settings.service.name -%}
{% if service != "vasypd" %}
restart_{{ service }}_service:
  service.running:
    - name: {{ service }}
    - reload: True
    - watch:
      - file: copy_map_file
{% endif -%}
{% endfor -%}
{% endif -%}
