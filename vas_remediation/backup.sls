# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vas_remediation/map.jinja" import vasypd_settings with context %}

{# ================================
 # Backup vasyp config files
 # ===============================#}
create_backup_dir:
  file.directory:
    - name: {{ vasypd_settings.backup.location }}
    - user: root
    - group: root
    - makedir: True

{% for file in vasypd_settings.backup.files %}
backup_vasyp_{{ file }}:
  cmd.run:
    - name: cp {{ file }}  {{ vasypd_settings.backup.location }}
    - require:
      - create_backup_dir
    - onlyif:
      - rpm -q {{ vas_remediation.pkg.name }}
{% endfor %}

backup_vasyp_version:
  cmd.run:
    - name: rpm -qa | grep vasyp > {{ vasypd_settings.backup.location }}/current_version
    - onlyif:
      - rpm -q {{ vasypd_settings.pkg.name }}

{% for __file in vasypd_settings.backup.mount_files %}
backup_mount_{{ __file }}:
  cmd.run:
    - name: cp /etc/{{ __file }}  {{ vasypd_settings.backup.location }}
    - require:
      - create_backup_dir
    - onlyif:
      - rpm -q {{ vas_remediation.pkg.name }}
{% endfor %}

